package com.headfwd.sintfwd;


public class PoemPrinter {
    private String line;
    private final boolean empty;

    PoemPrinter(String line) {
        this.line = line;
        empty = line.isEmpty();
    }

    void printLine() {
        if (empty) {
            System.out.println("\n---------------------------------------------------\n");
        } else {
            System.out.println(this.line);
        }
    }

    boolean lineEmpty(){
        return this.empty;
    }
}
