package com.headfwd.sintfwd;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@DisplayName("Test class that prints out Poems")
class PoemPrinterTest {
    
    @DisplayName("PoemPrinter#printLine, when empty prints line with dashes")
    @ParameterizedTest(name = "line {index} is empty: {1} ")
    @CsvFileSource(resources = "/Poem-jona.csv")
    void printLine(String line, boolean empty) {
        PoemPrinter printer = new PoemPrinter(line);
        printer.printLine();
        assertThat(printer.lineEmpty(), is(empty));
    }
}
